{-# LANGUAGE DeriveGeneric #-}

module Weather.Types (Weather(..), WeatherMain(..), WeatherCondition(..), conditionAwesomeIcon) where

import Data.Aeson
import Data.Char (toLower, chr)
import GHC.Generics
import Graphics.Icons.Weather
import Numeric (readHex)

lemonIcon (WeatherIcon x) = "%{T3}" <> [chr . fst . head . readHex $ x] <> "%{T-}"

data WeatherCondition = WeatherCondition {
  weatherConditionId :: Int
  , weatherConditionMain :: String
  , weatherConditionDescription :: String
  , weatherConditionIcon :: String
} deriving (Show, Eq, Generic)

instance FromJSON WeatherCondition where
  parseJSON = genericParseJSON defaultOptions {
                fieldLabelModifier = map toLower . drop 16 }

conditionAwesomeIcon :: String -> String
conditionAwesomeIcon "01d" = "☼"
conditionAwesomeIcon "01n" = "☾"
conditionAwesomeIcon "02d" = "⛅"
conditionAwesomeIcon "02n" = "☁"
conditionAwesomeIcon "03d" = "☁"
conditionAwesomeIcon "03n" = "☁"
conditionAwesomeIcon "04d" = "☁"
conditionAwesomeIcon "04n" = "☁"
-- conditionAwesomeIcon "09d" = "☂"
-- conditionAwesomeIcon "09n" = "☂"
-- conditionAwesomeIcon "10d" = "☔"
-- conditionAwesomeIcon "10n" = "☔"
conditionAwesomeIcon "09d" = lemonIcon dayRainCode
conditionAwesomeIcon "09n" = lemonIcon nightRainCode
conditionAwesomeIcon "10d" = lemonIcon dayRainCode
conditionAwesomeIcon "10n" = lemonIcon nightRainCode
conditionAwesomeIcon "11d" = "⛈"
conditionAwesomeIcon "11n" = "⛈"
conditionAwesomeIcon "13d" = "❄"
conditionAwesomeIcon "13n" = "❄"
conditionAwesomeIcon "50d" = "\xf0c2"
conditionAwesomeIcon "50n" = "\xf0c2"
conditionAwesomeIcon x = x

data WeatherMain = WeatherMain {
  temp :: Double
  , pressure :: Double
  , humidity :: Double
  , temp_min :: Double
  , temp_max :: Double
} deriving (Show, Eq, Generic)

instance FromJSON WeatherMain

data Weather = Weather {
  main :: WeatherMain
  , base :: String
  , dt :: Int
  , weather :: [WeatherCondition]
} deriving (Show, Eq, Generic)

instance FromJSON Weather
