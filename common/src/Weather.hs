{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Weather (didcotWeather, Weather(..), WeatherMain(..), WeatherCondition(..), conditionAwesomeIcon) where

import Data.Proxy
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.API
import Servant.Client

import Weather.Types

type WeatherAppId = String

type WeatherAPI =
  "weather" :> QueryParam' '[Required, Strict] "APPID" WeatherAppId :> QueryParam' '[Required, Strict] "id" Int :> QueryParam "units" Units :> Get '[JSON] Weather

data Units = Metric | Imperial

instance ToHttpApiData Units where
  toUrlPiece Imperial = "imperial"
  toUrlPiece Metric = "metric"

weatherAPI :: Proxy WeatherAPI
weatherAPI = Proxy

weatherByCityCode :: WeatherAppId -> Int -> Maybe Units -> ClientM Weather
weatherByCityCode = client weatherAPI

didcotWeather :: WeatherAppId -> IO (Either ServantError Weather)
didcotWeather appid = do
  manager' <- newManager defaultManagerSettings
  runClientM (weatherByCityCode appid 2651269 (Just Metric)) (mkClientEnv manager' (BaseUrl Http "api.openweathermap.org" 80 "/data/2.5/"))
