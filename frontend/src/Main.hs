{-# LANGUAGE FlexibleContexts, FlexibleInstances, RecursiveDo, OverloadedStrings #-}

module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (void)
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (liftIO, MonadIO)
import Data.Monoid
import Data.Time.Clock (getCurrentTime, NominalDiffTime)
import Data.Time.Format (formatTime, defaultTimeLocale)
import Reflex
import Reflex.Host.Basic
import System.IO ( IOMode(ReadMode), openFile, hGetLine, hClose, hFlush, stdout )

import qualified Weather as W



--- Stolen taffybar cpu code
toMB :: String -> Double
toMB size = (read size :: Double) / 1024

data MemoryInfo = MemoryInfo
  { memoryTotal :: Double
  , memoryFree :: Double
  , memoryBuffer :: Double
  , memoryCache :: Double
  , memorySwapTotal :: Double
  , memorySwapFree :: Double
  , memorySwapUsed :: Double -- swapTotal - swapFree
  , memorySwapUsedRatio :: Double -- swapUsed / swapTotal
  , memoryAvailable :: Double -- An estimate of how much memory is available for starting new apps
  , memoryRest :: Double -- free + buffer + cache
  , memoryUsed :: Double -- total - rest
  , memoryUsedRatio :: Double -- used / total
  }

emptyMemoryInfo :: MemoryInfo
emptyMemoryInfo = MemoryInfo 0 0 0 0 0 0 0 0 0 0 0 0

parseLines :: [String] -> MemoryInfo -> MemoryInfo
parseLines (line:rest) memInfo = parseLines rest newMemInfo
  where (label:size:_) = words line
        newMemInfo = case label of
                       "MemTotal:"     -> memInfo { memoryTotal = toMB size }
                       "MemFree:"      -> memInfo { memoryFree = toMB size }
                       "MemAvailable:" -> memInfo { memoryAvailable = toMB size }
                       "Buffers:"      -> memInfo { memoryBuffer = toMB size }
                       "Cached:"       -> memInfo { memoryCache = toMB size }
                       "SwapTotal:"    -> memInfo { memorySwapTotal = toMB size }
                       "SwapFree:"     -> memInfo { memorySwapFree = toMB size }
                       _               -> memInfo
parseLines _ memInfo = memInfo

parseMeminfo :: IO MemoryInfo
parseMeminfo = do
  s <- readFile "/proc/meminfo"
  let m = parseLines (lines s) emptyMemoryInfo
      rest = memoryFree m + memoryBuffer m + memoryCache m
      used = memoryTotal m - rest
      usedRatio = used / memoryTotal m
      swapUsed = memorySwapTotal m - memorySwapFree m
      swapUsedRatio = swapUsed / memorySwapTotal m
  return m { memoryRest = rest
           , memoryUsed = used
           , memoryUsedRatio = usedRatio
           , memorySwapUsed = swapUsed
           , memorySwapUsedRatio = swapUsedRatio
}
procData :: IO [Double]
procData = do
  h <- openFile "/proc/stat" ReadMode
  firstLine <- hGetLine h
  length firstLine `seq` return ()
  hClose h
  return (procParser firstLine)

procParser :: String -> [Double]
procParser = map read . tail . words

truncVal :: Double -> Double
truncVal v
  | isNaN v || v < 0.0 = 0.0
  | otherwise = v

-- | Return a pair with (user time, system time, total time) (read
-- from /proc/stat).  The function waits for 50 ms between samples.
cpuLoad :: IO Double
cpuLoad = do
  a <- procData
  threadDelay 50000
  b <- procData
  let dif = zipWith (-) b a
      tot = sum dif
      pct = map (/ tot) dif
      user = sum $ take 2 pct
      system = pct !! 2
      t = user + system
  -- return (truncVal user, truncVal system, truncVal t)
  return $ truncVal t
--- End stolen code

toUnicode :: Double -> Char
toUnicode x
  | x < 0.125 = '▁'
  | x < 0.25 = '▂'
  | x < 0.375 = '▃'
  | x < 0.5 = '▄'
  | x < 0.625 = '▅'
  | x < 0.75 = '▆'
  | x < 0.875 = '▇'
  | otherwise = '█'

class Widget t where
  fromWidget :: t -> String

instance Widget String where
  fromWidget = id

instance Widget Char where
  fromWidget = (:[])

instance Widget Double where
  fromWidget x = [toUnicode x]

instance (Widget x) => Widget [x] where
  fromWidget = concatMap fromWidget

instance (Widget x) => Widget (Sum x) where
  fromWidget (Sum x) = fromWidget x

backlog :: (Reflex t, MonadHold t m, MonadFix m) => Int -> Dynamic t a -> m (Dynamic t [a])
backlog n = scanDyn (replicate n) (\ new old -> take n $ new:old)

pollAction :: (Reflex t, TriggerEvent t m, PerformEvent t m, MonadIO (Performable m), MonadHold t m) => a -> IO a -> Event t b -> m (Dynamic t a)
pollAction begin act e = performEventAsync ((\fn -> liftIO $ fn =<< act) <$ e) >>= holdDyn begin

betterClock :: (PostBuild t m, PerformEvent t m, TriggerEvent t m, MonadIO (Performable m), MonadFix m) => NominalDiffTime -> m (Event t ())
betterClock diff = do
  clock <- void <$> tickLossyFromPostBuildTime diff
  postBuild <- getPostBuild
  return $ leftmost [clock, postBuild]

main :: IO ()
main = do
  apikey <- readFile "/home/adam/.config/openweathermaprc"
  basicHostForever $ mdo
    clock <- betterClock 5
    clock2 <- betterClock 600
    eCpu <- pollAction 0 cpuLoad clock
    eCpu2 <- backlog 8 eCpu
    eMem <- pollAction 0 (memoryUsedRatio <$> parseMeminfo) clock
    eMem2 <- backlog 8 eMem
    eWeather <- pollAction (Left "Not Loaded") (either (Left . show) Right <$> W.didcotWeather apikey) clock2
    eTime <- pollAction "Time" (formatTime defaultTimeLocale "\x231a %R" <$> getCurrentTime) clock
    let eCpu3 = "%{l} \xf108" <> fmap fromWidget eCpu2
                <> " \xf2db " <> fmap fromWidget eMem2
                <> eTime
                <> "%{S+}%{l}"
                <> fmap (either id $ W.conditionAwesomeIcon . W.weatherConditionIcon . head . W.weather) eWeather
                <> " \xf2c9 " <> fmap (either id $ show . W.temp . W.main) eWeather <> " °C "

    performEvent_ $ liftIO . (\ x -> putStrLn x >> hFlush stdout) <$> updated eCpu3 -- net
